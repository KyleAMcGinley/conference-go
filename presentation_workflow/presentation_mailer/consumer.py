import json
import pika
import django
import os
import sys
from django.core.mail import send_mail
import time
from pika.exceptions import AMQPConnectionError


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq"),
        )
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")

        def process_approval(ch, method, properties, body):
            print("approving")
            content = json.loads(body)
            presenter_name = content["presenter_name"]
            presenter_email = content["presenter_email"]
            presentation_title = content["title"]

            send_mail(
                subject="Your presentation has been accepted",
                message=f"{presenter_name}, we're happy to tell you that your presentation {presentation_title} has been accepted",
                from_email="admin@conference.go",
                recipient_list=[presenter_email],
                fail_silently=False,
            )

        def process_rejection(ch, method, properties, body):
            print("rejecting")
            content = json.loads(body)
            presenter_name = content["presenter_name"]
            presenter_email = content["presenter_email"]
            presentation_title = content["title"]

            send_mail(
                subject="Your presentation has been rejected",
                message=f"{presenter_name}, we're sorry to tell you that your presentation {presentation_title} has been rejected",
                from_email="admin@conference.go",
                recipient_list=[presenter_email],
                fail_silently=False,
            )

        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to queue")
        time.sleep(1)
