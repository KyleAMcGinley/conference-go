import requests
from .keys import pexels_api_key, weather_api_key


def get_location_photo(city, state):
    headers = {"Authorization": pexels_api_key}

    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    resp = requests.get(url, headers=headers)

    return resp.json()["photos"][0]["url"]


def get_weather_data(city, state):

    params = {"q": f"{city},{state},US", "appid": weather_api_key}
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()

    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": weather_api_key,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()

    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]

        return {"temp": temp, "description": desc}

    except (KeyError, IndexError):
        return None
